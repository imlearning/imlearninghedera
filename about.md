---
layout: page
title: About this Blog
permalink: /about/
published: true
---

<div class="page" markdown="1">

{% capture page_subtitle %}
<img
    class="me"
    alt="{{ author.name }}"
    src="{{ site.author.photo | relative_url }}"
    srcset="{{ site.author.photo2x | relative_url }} 2x"
/>
{% endcapture %}1

{% include page/title.html title=page.title subtitle=page_subtitle %}

## In January of 2023...

... I am relieved of the duties of my corporate job, along with about 7,000 other folks in my company. Rather than buy a motorcycle and drive all over the country (ask me about 1997), I decide to hang out a virutal shingle, at least temporarily, and apply my quarter century of tech writing experince to a new area. After a bit of back and forth, I decide on [Hedera](https://hedera.com/), about which I know nothing. I explain why I chose Hedera in [my first blog post](2023-01-09-welcome.md), but it boils down to my interest and to my belief that Hedera is a winner. I'm not a fan of wasting my efforts.

## Technical Details

I created this blog as a [Jekyll](https://jekyllrb.com/) site hosted on [GitLab](https://gitlab.com/). I chose GitLab because its [Groups](https://docs.gitlab.com/ee/user/group/) feature allows me to relate multiple blogs easily. Hedera is not the last new technology I'll be writing about in 2023.

For styling, I chose Marcin C Melangue's [dactyl](https://github.com/melangue/dactl) theme, which is provided as open source under the [MIT License](http://opensource.org/licenses/mit-license.php). I like that it is minimal, but not _too_ minimal, and looks great on Desktop and Mobile devices.

## Hero Graphic

The hero graphic for every post is by [James St. John](https://commons.wikimedia.org/wiki/File:Hedera_helix_%27Bulgaria%27_(Common_Ivy_cultivar)_1_(27707113969).jpg), CC BY 2.0, via [Wikimedia Commons](https://creativecommons.org/licenses/by/2.0)

![Hedera helix &#039;Bulgaria&#039; (Common Ivy cultivar) 1 (27707113969)](https://commons.wikimedia.org/wiki/File:Hedera_helix_%27Bulgaria%27_(Common_Ivy_cultivar)_1_(27707113969).jpg, "Hedera Ivy")
</div>