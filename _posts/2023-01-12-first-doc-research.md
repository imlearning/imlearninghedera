---
layout: post
title: 'Results of My Survey of Hedera Information'
date: 2023/01/12
tags:
  - hedera

description: >
   "I've picked the resources I'll study to learn about Hedera."

hero: https://imlearning.gitlab.io/imlearninghedera/assets/img/hedera-hero.jpg

overlay: green
published: true
---

When I was a developer, I didn't want to allocate too much time to background information. I tried to pick up what I needed along the way. If the technology I was working with was broad enough, or had a large support industry, it was often a chore to figure out what I should spend my valuable time reading.

<!--end_excerpt-->

With respect to Hedera, there's not a ton of information about it out
there, but there's always enough cruft to distract someone just
starting out.



## Identify the Resources

As Kesi Parker notes is his medium post, [How to Research: Top Tips
for Tech Writers](https://medium.com/technical-writing-is-easy/how-to-research-top-tips-for-tech-writers-52599edeeecf),
you have to figure your resources out. So I spent a couple of days
searching around for high-quality sources of truth about Hedera,
focusing on the things a developer needs to read or use to get
started. This survey was just to try and identify sources of truth --
I only went deep enough to judge the quality and purpose of the
content.

I looked for content that might be useful for ramping up and for
ongoing development. Background information is okay, but as I don't
want to get lost in rabbit holes, I only considered content that is
core to the technology. Something you'd read once or twice at most to
figure out where the technology fits in your technological world.

I came up with the following resources. I'll be reading and
familiarizing myself with them in the order I provide here. Once I go
through each, I'll be writing about what I've found. Because I have my
tech writer hat on for this, I'll be reading much more than I'll end
up recommending to you. 

Note that I have excluded code repositories for the time being. Code
repos are often the only source for meatfull content. But never to
worry -- I expect to sort through a ton of them in the not too distant
future. But during this go-round I'm doing what a tech writer does
with a new topic: familiarizing myself with the landscape of
information.

### Main Website

[https://hedera.com/](https://hedera.com/)

The website is the central source of truth about Hedera. 

> **Note:**  This is being written in January 2023. Websites cycle
> through organization and style often. Plus with the creation of
> the swirld.com company, some of the content will likely end up
> migrating.

I'm going to restrict my review of the following sections:

- Network section – I'll read everything.
- Devs section – I'll read everything.
- Use Cases – I'll read everything.
- Resources – This is a list of videos. I'm going to scan the list and view some of them. 
- Ecosystem – I'm going to scan this section to get an idea of the companies that have tossed their hats in the Hedera ring, so I can get a feel for the kinds of big bet projects that are in progress.

### Reddit Hedera Community

[https://www.reddit.com/r/Hedera/](https://www.reddit.com/r/Hedera/)

The Reddit community seems to focus on general info and opinion. There
are two good resource indices: _Social Media_ and _Developer
Resources_. Both seem to be up to date. There's a Getting Started
topic which looks to be more about getting started with the
subreddit. I will dig into it more deeply later.

I'll start out checking Reddit every day. Over the next couple of
weeks I'll get a feel for how often it's necessary to check into this
subreddit. Later in this blog series I'll provide my recommendations
on how often to peek into the several resources on Reddit, Discord,
YouTube, and Medium.

### YouTube Hedera Channel

[https://www.youtube.com/channel/UCIhE4NYpaX9E9SssFnwrjww](https://www.youtube.com/channel/UCIhE4NYpaX9E9SssFnwrjww)

There are a ton of videos here of all types. I'm going watch as many
as I can stand to try and build a top N list for developers.

### Discord Hedera Server

[https://discord.com/channels/373889138199494658/892945371901354076](https://discord.com/channels/373889138199494658/892945371901354076)

This is deeply developer focused. As with the Reddit community, I'll
start out with a daily review of the server channels to get a sense of
where the high-value content is.

### Medium 'Hedera' Topic

[https://medium.com/tag/hedera](https://medium.com/tag/hedera)

The Hedera foundation only publishes blog content on its main website,
but other folks post here periodically. I'm just going to look at
what's there and how often new content comes up.

### That's It
So there you have it -- that's my review work for the next couple of weeks at least. Thanks for reading!




