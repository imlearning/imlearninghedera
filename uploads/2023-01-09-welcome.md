---
layout: post
title: Welcome to the I'm Learning Hedera Blog
tags:
  - hedera
description: >
  The inagural post for the I'm Learning Hedera blog.

hero: > https://upload.wikimedia.org/wikipedia/commons/8/8b/Hedera_helix_%27Bulgaria%27_%28Common_Ivy_cultivar%29_1_%2827707113969%29.jpg

overlay: green
published: true
---

My first project for 2023 is to research and try to learn the [Hedera](https://hedera.com/) technology. I know almost nothing about it today, and I want to learn as much as possible as I can about it, write about what I learn, and then produce a good, simple, getting started guide by the end of the year. I plan to document my process of discovery in blog posts here (as needed, at least 2-3x a week) and sum those up in weekly posts on medium.com.

## Why Hedera?

I've chosen Hedera Hashgraph because it strikes me as an evolutionary advancement in blockchain technology. It appears to have solid funding and founding and has a decent sized multi-national community. So I don't think I'll be wasting my efforts on something really cool but destined for the shelf. By the end of the year (hopefully sooner), I'll know whether I'm on target with that impression or not. I'll also know whether I want to continue working with it, or to embark on a new learning adventure.

## How Will I Begin?

For no more than the first couple of weeks, I'm going to spend one or two hours a day trying to locate and rate all the news and content sources for Hedera. Ultimately, I want to locate the best resources for Hedera for developers who want to start working with the technology. I suspect there are probably already pleanty of fine docs. But having been a tech writer of developer documentation as long as I have, I know there's always an opportunity to clarify.

## Then What?
After the initial research, I'll start digging in. That'll be phase two.